const pay = {
  toWxPay: (params) => {
    console.log('微信支付 ==> ', params);
    uni.showLoading({
      title: '加载中'
    });
    return new Promise((resolve, reject) => {
      let prepayParam = {
        ...params,
        token: uni.getStorageSync('Token'),
        eCode: uni.getStorageSync('venId'),
      }
      http.post('/saler/api/v1/placeAnOrder', prepayParam).then((prepayRes) => {
        console.log(prepayRes);
        if (prepayRes.success) {
          let realPayParam = prepayRes.data || {};
          pay.toWxRealPay(realPayParam);
        } else {
          uni.hideLoading();
          reject('失败 2 ==> ', prepayRes);
        }
      }).catch(err => {
        uni.hideLoading();
        reject('失败 1 ==> ', err);
      })
    })
  },
  toWxRealPay: (realPayParam) => {
    uni.requestPayment({
      provider: 'wxpay',
      timeStamp: realPayParam.timeStamp,
      nonceStr: realPayParam.nonceStr,
      package: realPayParam.package,
      signType: realPayParam.signType,
      paySign: realPayParam.paySign,
      success: function(res) {
        console.log('success:' + JSON.stringify(res));
        uni.reLaunch({
          url: '/pages/pay-result/pay-result?sts=1',
          complete: () => {
            uni.hideLoading();
          }
        })
      },
      fail: function(err) {
        console.log('fail:' + JSON.stringify(err));
        uni.showToast({
          title: '支付失败',
          icon: 'error'
        });
        uni.hideLoading();
      }
    })
  }
}

export default pay
